package com.example.myapps

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.example.myapps.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    // use viewBinding to inflate mainActivity
    private val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }
    // bind our navHostFragment, which hosts the shibeGraph
    private val navHostFragment by lazy {
        supportFragmentManager.findFragmentById(R.id.fragment_host) as NavHostFragment
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        // set up the bottom nav drawer so that we can navigate when clicked
        binding.bottomNavigation.setupWithNavController(navHostFragment.navController)
    }
}
























