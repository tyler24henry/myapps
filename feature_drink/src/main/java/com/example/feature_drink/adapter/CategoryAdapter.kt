package com.example.feature_drink.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.feature_drink.databinding.ItemCategoryBinding
import com.example.feature_drink.model.response.CategoryDTO

class CategoryAdapter(
    // pass in higher order function
    private val categoryClicked: (category: CategoryDTO.CategoryItem) -> Unit
) : RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>() {

    private var categories = mutableListOf<CategoryDTO.CategoryItem>()

    // use our viewHolder's getInstance method to create the viewHolder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = CategoryViewHolder.getInstance(parent).apply {
        // itemView is a property on ViewHolder, which refers to the item_category.xml
        // we pass in the clicked category into our higher order function
        itemView.setOnClickListener { categoryClicked(categories[adapterPosition])}
    }

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        val category = categories[position]
        holder.loadCategory(category)
        // removed onClickListener from here, and moved to CategoryFragment -> used higher order function instead
//        holder.itemView.setOnClickListener { view ->
//            val directions = CategoryFragmentDirections.actionCategoryFragmentToCategoryDrinkFragment(category.strCategory)
//            view.findNavController().navigate(directions)
//        }
    }

    override fun getItemCount(): Int  = categories.size

    fun addCategories(categories: List<CategoryDTO.CategoryItem>) {
        this.categories.run {
            val oldSize = size
            clear()
            notifyItemRangeRemoved(0, oldSize)
            addAll(categories)
            notifyItemRangeInserted(0, categories.size)
        }
    }

    class CategoryViewHolder(private val binding: ItemCategoryBinding) : RecyclerView.ViewHolder(binding.root) {
        fun loadCategory(category: CategoryDTO.CategoryItem) = with(binding) {
            tvCategory.text = category.strCategory
        }
        companion object {
            fun getInstance(parent: ViewGroup)  = ItemCategoryBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ).let { CategoryViewHolder(it) }
        }
    }
}




























