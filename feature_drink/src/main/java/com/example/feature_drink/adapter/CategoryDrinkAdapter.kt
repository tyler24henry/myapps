package com.example.feature_drink.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.feature_drink.databinding.ItemCategoryDrinkBinding
import com.example.feature_drink.model.response.CategoryDrinksDTO

class CategoryDrinkAdapter(
    private val categoryDrinks: List<CategoryDrinksDTO.Drink>,
    private val categoryDrinkClicked: (CategoryDrinksDTO.Drink) -> Unit
    ) : RecyclerView.Adapter<CategoryDrinkAdapter.CategoryDrinkViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup, viewType: Int
    ) = CategoryDrinkViewHolder.getInstance(parent).apply {
        itemView.setOnClickListener { categoryDrinkClicked(categoryDrinks[adapterPosition]) }
    }

    override fun onBindViewHolder(holder: CategoryDrinkViewHolder, position: Int) {
        val categoryDrink = categoryDrinks[position]
        holder.loadCategoryDrink(categoryDrink)
    }

    override fun getItemCount(): Int  = categoryDrinks.size

//    fun addCategoryDrinks(categoryDrinks: List<CategoryDrinksDTO.Drink>) {
//        this.categoryDrinks.run {
//            val oldSize = size
//            clear()
//            notifyItemRangeRemoved(0, oldSize)
//            addAll(categoryDrinks)
//            notifyItemRangeInserted(0, categoryDrinks.size)
//        }
//    }

    class CategoryDrinkViewHolder(private val binding: ItemCategoryDrinkBinding) : RecyclerView.ViewHolder(binding.root) {
        fun loadCategoryDrink(categoryDrink: CategoryDrinksDTO.Drink) = with(binding) {
            tvCategoryDrink.text = categoryDrink.strDrink

            // glide
//            Glide.with(root.context).load(categoryDrink.strDrinkThumb).into(ivCategoryDrink)

            // picasso
//            Picasso.get().load(categoryDrink.strDrinkThumb).into(ivCategoryDrink)

            // coil
            ivCategoryDrink.load(categoryDrink.strDrinkThumb)
        }

        companion object {
            fun getInstance(parent: ViewGroup) = ItemCategoryDrinkBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ).let { binding -> CategoryDrinkViewHolder(binding) }
        }
    }
}




















