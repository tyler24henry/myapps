package com.example.feature_drink.view.category

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.feature_drink.adapter.CategoryAdapter
import com.example.feature_drink.databinding.FragmentCategoryBinding
import com.example.feature_drink.model.response.CategoryDTO
import com.example.feature_drink.viewmodel.CategoryViewModel

class CategoryFragment : Fragment() {
    private var _binding: FragmentCategoryBinding? = null
    private val binding get() = _binding!!
    private val categoryViewModel by viewModels<CategoryViewModel>()
    private val categoryAdapter by lazy { CategoryAdapter(::categoryClicked) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentCategoryBinding.inflate(inflater, container, false).also { _binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.rvCategories.layoutManager = LinearLayoutManager(context)
        binding.rvCategories.adapter = categoryAdapter

        categoryViewModel.state.observe(viewLifecycleOwner) { categoryState ->
            // binding.loader.isVisible = categoryState.isLoading -> didn't make a loader, but this would be how you set its visibility
            categoryAdapter.addCategories(categories = categoryState.categories)
            // binding.rvCategories.adapter = CategoryAdapter().apply { addCategories(categories = categoryState.categories) }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    fun categoryClicked(category: CategoryDTO.CategoryItem) {
//        val directions = CategoryFragmentDirections.actionCategoryFragmentToCategoryDrinkFragment(category.strCategory)
//        findNavController().navigate(directions)
    }
}






