package com.example.feature_drink.view.category

import com.example.feature_drink.model.response.CategoryDrinksDTO

data class CategoryDrinkState(
    val isLoading: Boolean = false,
    val categoryDrinks: List<CategoryDrinksDTO.Drink> = emptyList()
)
