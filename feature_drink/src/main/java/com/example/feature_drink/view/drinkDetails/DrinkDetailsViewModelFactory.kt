package com.example.feature_drink.view.drinkDetails

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.feature_drink.viewmodel.DrinkDetailsViewModel

class DrinkDetailsViewModelFactory(private val id: Int) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return DrinkDetailsViewModel(id) as T
    }
}