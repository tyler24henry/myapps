package com.example.feature_drink.view.category

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.feature_drink.adapter.CategoryDrinkAdapter
import com.example.feature_drink.viewmodel.CategoryDrinkViewModel
import com.example.feature_drink.databinding.FragmentCategoryDrinkBinding
import com.example.feature_drink.viewmodel.CategoryViewModel

class CategoryDrinkFragment : Fragment() {
    private var _binding: FragmentCategoryDrinkBinding? = null
    private val binding get() = _binding!!
    private val categoryViewModel by viewModels<CategoryViewModel>()
//    private val args by navArgs<CategoryDrinkFragmentArgs>()
    // use lambda function to pass argument into viewModel, to set the category we want to query from
    private val categoryDrinkViewModel by viewModels<CategoryDrinkViewModel>() {
//        CategoryDrinkViewModelFactory(args.category)
    CategoryDrinkViewModelFactory("Cocktail")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentCategoryDrinkBinding.inflate(inflater, container, false).also { _binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

//        categoryViewModel.getCategoryDrinks(args.category)
        categoryViewModel.getCategoryDrinks("Cocktail")
        binding.rvCategoryDrinks.layoutManager = LinearLayoutManager(context)
//        categoryViewModel.categoryDrinkState.observe(viewLifecycleOwner) { categoryDrinkState ->
//            binding.rvCategoryDrinks.adapter = CategoryDrinkAdapter().apply { addCategoryDrinks(categoryDrinks = categoryDrinkState.categoryDrinks) }
//        }

        categoryDrinkViewModel.state.observe(viewLifecycleOwner) { categoryDrinkState ->
            if (categoryDrinkState.categoryDrinks.isNotEmpty()) {
                binding.rvCategoryDrinks.adapter = CategoryDrinkAdapter(categoryDrinkState.categoryDrinks) { categoryDrink ->
                    // this is our onClickListener (for each item in the recycler view, we attach an onClickListener)
                    // navigate to fragment
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
















