package com.example.feature_drink.view.drinkDetails

import com.example.feature_drink.model.response.DrinkDetailsDTO

data class DrinkDetailsState(
    val isLoading: Boolean = false,
    val drinkDetails: List<DrinkDetailsDTO.Drink> = emptyList()
)
