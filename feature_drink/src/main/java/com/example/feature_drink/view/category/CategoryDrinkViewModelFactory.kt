package com.example.feature_drink.view.category

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.feature_drink.viewmodel.CategoryDrinkViewModel

// this lets us override how the viewModel is created
// we do this because we want to pass an argument into the viewModel when we instantiate it
class CategoryDrinkViewModelFactory(private val category: String) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return CategoryDrinkViewModel(category) as T
    }
}