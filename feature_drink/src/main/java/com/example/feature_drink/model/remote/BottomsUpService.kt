package com.example.feature_drink.model.remote

import com.example.feature_drink.model.response.CategoryDTO
import com.example.feature_drink.model.response.CategoryDrinksDTO
import com.example.feature_drink.model.response.DrinkDetailsDTO
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import retrofit2.http.GET
import retrofit2.http.Query

interface BottomsUpService {

    companion object {
        private const val BASE_URL = "https://www.thecocktaildb.com"
        private const val QUERY_CATEGORY = "c"
        private const val QUERY_ID = "i"

        fun getInstance() : BottomsUpService {
            val retrofit : Retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
            // return retrofit.create(BottomsUpService::class.java)
            return retrofit.create()
        }
    }

    @GET("/api/json/v1/1/list.php")
    suspend fun getCategories(@Query(QUERY_CATEGORY) type: String = "list") : CategoryDTO

    @GET("/api/json/v1/1/filter.php")
    suspend fun getCategoryDrinks(@Query(QUERY_CATEGORY) category: String = "filter") : CategoryDrinksDTO

    @GET("/api/json/v1/1/lookup.php")
    suspend fun getDrinkDetails(@Query(QUERY_ID) drinkId: Int = 11007) : DrinkDetailsDTO
}