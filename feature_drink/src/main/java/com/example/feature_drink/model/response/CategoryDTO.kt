package com.example.feature_drink.model.response

import com.google.gson.annotations.SerializedName

data class CategoryDTO(
    // serialized name is the name of the gson key
    // we have to specify its name is "drinks" because we changed our val to "categoryItems"
    @SerializedName("drinks")
    val categoryItems: List<CategoryItem>
) {
    data class CategoryItem(
        val strCategory: String
    )
}
