package com.example.feature_drink.model

import com.example.feature_drink.model.remote.BottomsUpService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object BottomsUpRepo {

    private val bottomsUpService by lazy { BottomsUpService.getInstance() }

    suspend fun getCategories()  = withContext(Dispatchers.IO) {
        bottomsUpService.getCategories()
    }

    suspend fun getCategoryDrinks(category: String) = withContext(Dispatchers.IO) {
        bottomsUpService.getCategoryDrinks(category)
    }

    suspend fun getDrinkDetails(id: Int) = withContext(Dispatchers.IO) {
        bottomsUpService.getDrinkDetails(id)
    }
}