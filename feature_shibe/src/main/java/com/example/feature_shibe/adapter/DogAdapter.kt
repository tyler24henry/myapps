package com.example.feature_shibe.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.feature_shibe.databinding.ItemDogBinding
import com.example.feature_shibe.model.local.entity.Shibe

class DogAdapter(
    private val dogs: List<Shibe>,
    private val dogClicked: (dogStr: String) -> Unit,
) : RecyclerView.Adapter<DogAdapter.DogViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup, viewType: Int
    ): DogViewHolder = DogViewHolder.getInstance(parent).apply {
        itemView.setOnClickListener { dogClicked(dogs[adapterPosition].url) }
    }

    override fun onBindViewHolder(holder: DogViewHolder, position: Int) {
        holder.loadDog(dogs[position])
    }

    override fun getItemCount(): Int = dogs.size

    class DogViewHolder(private val binding: ItemDogBinding) : RecyclerView.ViewHolder(binding.root) {
        fun loadDog(shibe: Shibe) = with(binding) {
            ivDog.load(shibe.url)
        }
        companion object {
            fun getInstance(parent: ViewGroup) = ItemDogBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ).let { binding -> DogViewHolder(binding) }
        }
    }
}





















