package com.example.feature_shibe.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.example.feature_shibe.model.DogRepo
import com.example.feature_shibe.view.dog.DogState

class DogViewModel(count: Int, private val dogRepo : DogRepo) : ViewModel() {

    val state: LiveData<DogState> = liveData {
        emit(DogState(isLoading = true))
        val dogs = dogRepo.getDogs(count)
        emit(DogState(dogs = dogs))
    }
}





























