package com.example.feature_shibe.model

import android.content.Context
import com.example.feature_shibe.model.local.ShibeDatabase
import com.example.feature_shibe.model.local.entity.Shibe
import com.example.feature_shibe.model.remote.DogService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class DogRepo(context: Context) {
    private val dogService = DogService.getInstance()
    private val shibeDao = ShibeDatabase.getInstance(context).shibeDao()

    suspend fun getDogs(count: Int) = withContext(Dispatchers.IO) {
        val cachedShibes: List<Shibe> = shibeDao.getAll()

        // if there aren't any shibes in the db yet, insert them, and then query the db and return them
        return@withContext cachedShibes.ifEmpty {
            // get the shibe urls
            val shibeUrls: List<String> = dogService.getDogs(count)
            // convert the urls into shibe objects
            val shibes: List<Shibe> = shibeUrls.map { Shibe(url = it) }
            // insert the shibe objects into the db
            shibeDao.insert(shibes)
            return@ifEmpty shibes
        }
    }
}




























