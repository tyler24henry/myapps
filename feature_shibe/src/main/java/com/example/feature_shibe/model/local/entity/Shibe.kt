package com.example.feature_shibe.model.local.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "shibe_table") // if didn't rename, would just be class name
data class Shibe(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    val url: String,
)
