package com.example.feature_shibe.model.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.feature_shibe.model.local.entity.Shibe

@Dao
interface ShibeDao {

    @Query("select * from shibe_table")
    suspend fun getAll(): List<Shibe>

    @Insert
    suspend fun insert(shibe: List<Shibe>)
}