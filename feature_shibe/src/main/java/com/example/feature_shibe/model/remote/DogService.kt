package com.example.feature_shibe.model.remote

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import retrofit2.http.GET
import retrofit2.http.Query

interface DogService {

    companion object {
        private const val BASE_URL = "https://shibe.online/"
        private const val COUNT_QUERY = "count"

        fun getInstance() : DogService {
            val retrofit : Retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            return retrofit.create()
        }
    }

    @GET("api/shibes")
    suspend fun getDogs(@Query(COUNT_QUERY) count: Int = 1): List<String>
}




















