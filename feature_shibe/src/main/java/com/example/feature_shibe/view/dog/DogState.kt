package com.example.feature_shibe.view.dog

import com.example.feature_shibe.model.local.entity.Shibe

data class DogState(
    val isLoading: Boolean = false,
    val dogs: List<Shibe> = emptyList()
)
