package com.example.feature_shibe.view.dog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.feature_shibe.adapter.DogAdapter
import com.example.feature_shibe.databinding.FragmentDogBinding
import com.example.feature_shibe.model.DogRepo
import com.example.feature_shibe.viewmodel.DogViewModel

class DogFragment : Fragment() {
    private var _binding: FragmentDogBinding? = null
    private val binding get() = _binding!!
    private val dogViewModel by viewModels<DogViewModel>() {
        DogViewModelFactory(10, DogRepo(requireContext()))
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentDogBinding.inflate(inflater, container, false).also { _binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        dogViewModel.state.observe(viewLifecycleOwner) { state ->
            if (state.dogs.isNotEmpty()) {
                binding.rvDogs.adapter = DogAdapter(dogs = state.dogs) { dog ->
                    Toast.makeText(context, "${dog.toString()}", Toast.LENGTH_LONG).show()
                }
            }
        }

        binding.btnChangeRv.setOnClickListener {
            val currentLayout = binding.rvDogs.layoutManager.toString()

            if (currentLayout.contains("StaggeredGridLayoutManager")) {
                binding.rvDogs.layoutManager = LinearLayoutManager(context)
            } else if (currentLayout.contains("LinearLayoutManager")) {
                binding.rvDogs.layoutManager = GridLayoutManager(context, 2)
            } else {
                binding.rvDogs.layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}