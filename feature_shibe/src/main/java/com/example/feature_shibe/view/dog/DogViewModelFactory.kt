package com.example.feature_shibe.view.dog

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.feature_shibe.model.DogRepo
import com.example.feature_shibe.viewmodel.DogViewModel

class DogViewModelFactory(
        private val count: Int,
        private val shibeRepo: DogRepo
    ) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return DogViewModel(count, shibeRepo) as T
    }
}






























